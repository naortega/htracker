/*
 * Copyright (C) 2022 Ortega Froysa, Nicolás <nicolas@ortegas.org>
 * Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::path::PathBuf;
use std::fs::OpenOptions;
use std::io::{BufReader, BufWriter};
use time::Weekday;

use crate::habit::Habit;

pub struct HabitMgr
{
	habits:Vec<Habit>,
	habits_path:PathBuf,
}

impl HabitMgr
{
	pub fn new(data_dir:&PathBuf) -> Self
	{
		let habits_path:PathBuf = data_dir.clone().join("active_habits.json");
		/*
		 * There may be a way of simplifying this, but the idea is that if we try
		 * to read the file without any JSON code in it then we'll get an error.
		 * This code avoids said error.
		 */
		if !habits_path.is_file()
		{
			let habits_file = OpenOptions::new()
				.create(true)
				.write(true)
				.open(&habits_path)
				.unwrap_or_else(|e| {
					panic!("Error creating file {}:\n{}",
								 habits_path.display(), e);
				});
			let habit_writer = BufWriter::new(&habits_file);
			serde_json::to_writer(habit_writer, &Vec::<Habit>::new())
				.unwrap_or_else(|e| {
					panic!("Error writing to file {}:\n{}",
								 habits_path.display(), e);
				});
		}

		/*
		 * The list of active habits is necessary for all functions, thus they're
		 * imported here, allowing member functions which only wish to access
		 * information to be constant.
		 */
		let habits_file = OpenOptions::new()
			.read(true)
			.open(&habits_path)
			.unwrap_or_else(|e| {
				panic!("Error opening file {}:\n{}", habits_path.display(), e);
			});
		let habits_reader = BufReader::new(&habits_file);

		let habits:Vec<Habit> = serde_json::from_reader(habits_reader).unwrap();

		Self
		{
			habits,
			habits_path,
		}
	}

	fn export_habits(&self)
	{
		let habits_file = OpenOptions::new()
			.truncate(true)
			.write(true)
			.open(&self.habits_path)
			.unwrap_or_else(|e| {
				panic!("Error opening file {}:\n{}", self.habits_path.display(), e);
			});
		let habits_writer = BufWriter::new(habits_file);
		serde_json::to_writer(habits_writer, &self.habits)
			.unwrap_or_else(|e| {
					panic!("Error writing to file {}:\n{}",
								 self.habits_path.display(), e);
			});
	}

	fn days_array_from_string(days:String) -> [bool;7]
	{
		let days_list = days.split(",");
		let mut days_active:[bool;7] = [ false; 7 ];
		for i in days_list
		{
			match i
			{
				"mon" =>
					days_active[Weekday::Monday.number_days_from_monday() as usize] = true,
				"tue" =>
					days_active[Weekday::Tuesday.number_days_from_monday() as usize] = true,
				"wed" =>
					days_active[Weekday::Wednesday.number_days_from_monday() as usize] = true,
				"thu" =>
					days_active[Weekday::Thursday.number_days_from_monday() as usize] = true,
				"fri" =>
					days_active[Weekday::Friday.number_days_from_monday() as usize] = true,
				"sat" =>
					days_active[Weekday::Saturday.number_days_from_monday() as usize] = true,
				"sun" =>
					days_active[Weekday::Sunday.number_days_from_monday() as usize] = true,
				_ =>
					panic!("Day {} not recognized!", i),
			}
		}

		return days_active;
	}

	pub fn add(&mut self, name:String, bad:bool, priority:char, days:String)
	{
		let days_active = HabitMgr::days_array_from_string(days);
		self.habits.push(Habit::new(name.clone(), bad, priority, days_active));
		self.export_habits();

		println!("New habit {} added.", &name);
	}

	pub fn delete(&mut self, id:usize)
	{
		let old_habit = self.habits.remove(id);
		self.export_habits();
		println!("Removed habit {}", old_habit.get_name());
	}

	pub fn habit_info(&self, id:usize)
	{
		let habit = &self.habits[id];
		println!("Name: {}", habit.get_name());
		println!("ID: {}", id);
		println!("UID: {}", habit.get_uid());
		println!("Active Days: {}", habit.active_days_string());
		println!("Bad: {}", habit.get_bad());
		println!("Weight: {}", habit.get_priority());
		println!("Done: {}", habit.get_done());
		println!("Streak: {}", habit.get_streak());
	}

	pub fn modify(&mut self, id:usize,
						 name:Option<String>,
						 toggle_bad:bool,
						 priority:Option<char>,
						 days:Option<String>)
	{
		if name.is_some()
		{
			self.habits[id].set_name(name.unwrap());
		}
		if toggle_bad
		{
			let is_bad = self.habits[id].get_bad();
			self.habits[id].set_bad(!is_bad);
		}
		if priority.is_some()
		{
			self.habits[id].set_priority(priority.unwrap());
		}
		if days.is_some()
		{
			let days_active = HabitMgr::days_array_from_string(days.unwrap());
			self.habits[id].set_days(days_active);
		}
		self.export_habits();
	}

	pub fn list(&self, all:bool)
	{
		if self.habits.is_empty()
		{
			println!("There are no habits. Add one!");
		}
		else if !all && self.habits.iter().all(|i| !i.active_today())
		{
			println!("No active habits available today!");
		}
		else
		{
			println!(" {0: <3} | {1: <5} | {2: <8} | {3: <5} | {4: <6} | {5}",
							 "id", "bad", "priority", "done", "streak", "name");
			for (i, habit) in self.habits.iter().enumerate()
			{
				if all || habit.active_today()
				{
					println!(" {0: <3} | {1: <5} | {2: <8} | {3: <5} | {4: <6} | {5}",
									 i,
									 habit.get_bad(),
									 habit.get_priority(),
									 habit.get_done(),
									 habit.get_streak(),
									 habit.get_name());
				}
			}
		}
	}
}
