/*
 * Copyright (C) 2022 Ortega Froysa, Nicolás <nicolas@ortegas.org>
 * Author: Ortega Froysa, Nicolás <nicolas@ortegas.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use uuid::Uuid;
use serde::{Serialize, Deserialize};
use time::OffsetDateTime;

#[derive(Serialize, Deserialize, Debug)]
pub struct Habit
{
	uid:String,
	name:String,
	bad:bool,
	priority:char,
	// Day 0 is Monday. Use number_days_from_monday() to determine index.
	days_active:[bool;7],
	done:bool,
	streak:i32,
}

impl Habit
{
	pub fn new(name:String, bad:bool, priority:char, days_active:[bool;7]) -> Self
	{
		if ![ 'H','L','M' ].contains(&priority)
		{
			panic!("Unknown priority {}", priority);
		}
		Self
		{
			uid: Uuid::new_v4().hyphenated().to_string(),
			name,
			bad,
			priority,
			days_active,
			done: false,
			streak: 0,
		}
	}

	pub fn active_days_string(&self) -> String
	{
		let mut res:String = String::new();

		for (i,v) in self.days_active.iter().enumerate()
		{
			if *v
			{
				res.push_str(match i {
					0 => "mon",
					1 => "tue",
					2 => "wed",
					3 => "thu",
					4 => "fri",
					5 => "sat",
					6 => "sun",
					_ => "unk", // this one will never occur, but the compiler complains
				});
				res.push(',');
			}
		}
		res.pop();

		return res;
	}

	pub fn active_today(&self) -> bool
	{
		return self.days_active[OffsetDateTime::now_local().unwrap()
			.weekday().number_days_from_monday() as usize]
	}

	pub fn get_uid(&self) -> &String { &self.uid }
	pub fn get_name(&self) -> &String { &self.name }
	pub fn get_bad(&self) -> bool { self.bad }
	pub fn get_priority(&self) -> char { self.priority }
	pub fn get_done(&self) -> bool { self.done }
	pub fn get_streak(&self) -> i32 { self.streak }

	pub fn set_name(&mut self, name:String) { self.name = name; }
	pub fn set_bad(&mut self, bad:bool) { self.bad = bad; }
	pub fn set_priority(&mut self, priority:char) { self.priority = priority; }
	pub fn set_days(&mut self, days:[bool;7]) { self.days_active = days; }
	pub fn set_done(&mut self, done:bool) { self.done = done; }
	// TODO: whether set_streak or inc/dec/reset_streak
}
