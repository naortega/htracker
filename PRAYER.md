# Project Prayer

Lord God, you who knows the weakness of Men, and that only by cooperation with
your grace may we overcome the shackles of sin, bless the users of this program
that by a disciplined adherence to the habits they have set forth to form and to
reject, that they may succeed in their enterprise and grow in both faith &
virtue. In the name of Jesus Christ our Lord. Amen.
