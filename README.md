# Habit Tracker

Forming good habits and getting rid of bad ones can be difficult. The objective
of this piece of software is to be able to track our habits to make sure we're
progressing.

## Usage

Habit Tracker uses commands appended after the program name in the shell to use
different functionalities (i.e. `htracker <command> [options]`). If no command
is specified then the `list` command is assumed by default. Here are the list of
commands:

- `add` or `a`: add a new habit
- `commit`: commit to having done a habit
- `delete` or `del`: delete a habit
- `info` or `i`: get information about a habit
- `help` or `h`: show help information
- `list` or `ls`: list the habits available for today
- `modify` or `mod`: modify the settings for a habit
- `stats`: show statistics on current habits
- `vacation` or `vac`: toggle vacation mode

## License

This software is licensed under the terms & conditions of the GNU Affero
General Public License version 3 or greater (see the [license file](./LICENSE)).
